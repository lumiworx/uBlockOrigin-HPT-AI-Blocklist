# Code of Conduct - Effective: May 2nd, 2024

## 1. Purpose

One of the primary goals of this project is to be inclusive to the largest number of contributors and data consumers with a wide and varied range of diverse backgrounds. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion (or lack thereof).

This code of conduct outlines our expectations for all those who participate in any manner, as well as the consequences for their unacceptable behavior as they do.

We invite all those who participate in this project to help us create safe and positive experiences for everyone contributing to, and consuming what is offered.

## 2. Open Source Citizenship

A supplemental goal of this Code of Conduct is to increase open source citizenship by encouraging participants to recognize and strengthen the relationships between our actions and their effects on other contributors and/or commentors.

Communities mirror the societies in which they exist and positive action is essential to counteract the many forms of inequality and abuses of power that exist in society.

If you see someone who is not making a reasonable effort to ensure our community is welcoming, friendly, and encourages all participants to contribute to the fullest extent, we want to know.

## 3. Expected Behavior

The following behaviors are expected and requested of all contributors and/or commentors:

  * Participate in an authentic and active way. In doing so, you contribute to the health and longevity of this project.
  * Exercise consideration and respect in your speech and actions.
  * Attempt collaboration before conflict.
  * Refrain from demeaning, discriminatory, or harassing behavior and speech.
  * Be mindful of your virtual surroundings and of your fellow participants. Alert the maintainer if you notice a dangerous situation, someone in distress, or violations of this Code of Conduct, even if they seem inconsequential.

## 4. Unacceptable Behavior

The following behaviors are considered harassment and are unacceptable from project contributors and/or commentors:

  * Insinuation or threats of violence or violent language directed against another person.
  * Sexist, racist, homophobic, transphobic, ableist or otherwise discriminatory jokes and language.
  * Posting or displaying sexually explicit or violent material.
  * Posting or threatening to post other people's personally identifying information ("doxing").
  * Personal insults, particularly those related to gender, sexual orientation, race, religion, or disability, or skill level.
  * Unwelcome sexual attention. This includes, sexualized comments or jokes and unwelcomed sexual communications or advances.
  * Deliberate intimidation, stalking or following (online or in person).
  * Advocating for, or encouraging, any of the above behavior.
  * Sustained disruption of project issues and/or resolutions and/or data verification or acquisition.

## 5. Consequences of Unacceptable Behavior

Unacceptable behavior from any contributor and/or commentor, including sponsors and those with decision-making authority, will not be tolerated.

Anyone asked to stop unacceptable behavior is expected to comply immediately.

If a contributor and/or commentor engages in unacceptable behavior, the project's organizers may take any action they deem appropriate, up to and including temporary bans or permanent expulsion from the project without warning.

## 6. Reporting Guidelines

If you are subject to or witness unacceptable behavior, or have any other concerns, please notify the maintainer as soon as possible using the contact details outlined below.

## 7. Addressing Grievances

If you feel you have been falsely or unfairly accused of violating this Code of Conduct, you should notify the maintainer with a concise description of your grievance. Your grievance will be handled in accordance within these guidlines, as well as the overriding policies of Codeberg.com.

## 8. Scope

We expect all project participants (contributors, paid or otherwise; sponsors; and other guests and/or commentors) to abide by this Code of Conduct in all project venues, as well as in all one-on-one communications arising from the project.

This code of conduct and its related procedures also applies to unacceptable behavior occurring outside the scope of community activities when such behavior has the potential to adversely affect the safety and wellbeing of contributors, commentors, or sponsors.

## 9. Reporting Conduct Issues

Contact info

![codeberg(dot)ops(AT)lumiworx(dot)com](https://lumiworx.com/storage/resources/codeberg-ops-email.jpg "Contact Email Address")

#### If reporting conduct issues:

* If you believe someone is breaking this code of conduct, you may reply to them, and point to this project's specific code of conduct repository file - i.e., this file, "CODE_OF_CONDUCT.md". Such messages may be in public or in private, whatever is most appropriate. Assume some good faith at the onset; as it is possible that participants are unaware of their bad behaviour; or it may simply be a language translation issue. Should there be difficulties in dealing with the situation, you may report your concerns to the maintainer, at the above email address. Serious or persistent offenders will be banned, and/or reported to Codeberg and/or other appropriate authorities where and when appropriate.
* Please keep copies of any physical or digital materials to detail any inappropriate conduct in depth and breadth. In cases where the offending communications at issue happened outside the public view, these will likely be required to be provided as a followup to your initial report.
* Please use a valid email as your recipient address that is **_not_** a "throw away" or temporary mailbox. The reporting email address posted above will only process project conduct issue resolutions only, and no mail routing for complaints will take place for 'invalid' addresses.

## 10. License and attribution

The original "Citizen Code of Conduct" is distributed by [Stumptown Syndicate](http://stumptownsyndicate.org) under a [Creative Commons Attribution-ShareAlike license](http://creativecommons.org/licenses/by-sa/3.0/), and is the foundational basis of this document.

Portions of this text is also derived from the [Django Code of Conduct](https://www.djangoproject.com/conduct/) and the [Geek Feminism Anti-Harassment Policy](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy).

the Original "Citizens Code of Conduct" versions and revisions:

_Revision 2.3. Posted 6 March 2017._ | _Revision 2.2. Posted 4 February 2016._ | _Revision 2.1. Posted 23 June 2014._ | _Revision 2.0, adopted by the [Stumptown Syndicate](http://stumptownsyndicate.org) board on 10 January 2013. Posted 17 March 2013._