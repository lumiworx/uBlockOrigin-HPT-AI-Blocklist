# Changelist

### Original Fork
- The 'main' branch was a straight clone/commit of the original, with no additions or changes to the blocklists themselves.
- Some initial Wiki pages were created with explanations and descriptions of filetypes and methodology.

### lumiworx-dev
- Initiates a new 'Dev' branch, that will begin the process of splitting the original files into more 'even-handed' blocklistings, to be more discrimenanting on what is blocked.
- The README was modified to reflect the future direction of this project, and details on the introduction of the 'lumiworx-dev' branch and 'HPT' file types, in general.
- Added a series of 'distilled' lists from the original project in HOSTS formated entries to adapt and convert the lists into a format accepted by individual devices at their base level, and to facilitate their inclusion into network-wide DNS Sinkholes that accept HOSTS file entries, i.e., PiHole and Technitium, etc.
- Added a LICENSE file to reflect placing all future derivations and additions under UnLicense terms, and firmly into the Public Domain.
- Added a summary of the repository's contents to specify the scope of blocking targets within each file.

### Planned Development
- At some point, the original uB file types may be split into similar categories used in the 'HPT' files, or they may be dropped entirely, leaving only the HPT conversions and their later iterations. That's undecided for now.

#### May 2nd, 2024:
Added "CODE OF CONDUCT" language to project.

#### June 3rd, 2024:
Removing all uB files from project after Google's "Manifest V3" changes, and continued push to restrict blocker/filter funtionality in Chromium-based browsers [See: https://www.pcmag.com/news/rip-ublock-origin-google-proceeds-with-plan-to-shake-up-chrome-extensions]