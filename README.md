# 'HPT' - HOSTS/PiHole/Technitium - AI Blocklists

This repository provides a series of blocklists (cumulatively ~ 1100+ entries) dealing with generative AI and AI generated content. These files offer site blocking files formatted and adapted for use in desktop, server, and appliance environments that can process blacklist entries in Host file format. All "HPT" labeled files are compatible for use with most desktop operating systems, as well as HOST/PiHole/Technitium and similar DNS services and servers, that allow for site/domain/resource blocking and blocklists.

##### If you are curious about website domain blocking at the server level, AKA a DNS Sinkhole, there is a downloadable whitepaper from the SANS institute here: https://www.sans.org/white-papers/33523/

## Selecting and using "HPT" labeled files

You can elect to use either the 'Full' list to include all 'AI' domain subcategory types for blocking -OR- use the individual files to selectively target a particular type of AI resource. It is NOT necessary to use both the 'Full' and the individualized subcategory files. Using all of them would cause duplication for some or all of the entries, and would be counterproductive to the lookup speed for DNS queries.

Sites and resources listed in the "HPT" files will only point a connection request for an AI domain to a purposefully unresolvable IPv4 address of 0.0.0.0. Essentially the request is not directed to any physical or virtual host on the Internet, and fails gracefully when no real connection is possible.

## How to include the "HPT" blocklist w/ PC/MAC/Linux HOST files

As long as you have the proper permissions to edit files that are 'owned' by the Operating System, you can amend an existing HOST file, or create and use a new one, if one doesn't currently exist. You can download any appropriate file listed here that includes 'HPT' in the filename, open it in your choice of text editors, and then copy/paste the text onto the end of whatever entries you currently have/use, and save the new or amended list. These entries allow blocking any added entries for any user of that individual device. If you want or need the same internet blocking capability for multiple devices on an entire network, you may want to consider a universal solution by using a dedicated DNS server that can offer one point of setup and maintenance (i.e., [PiHole](https://pi-hole.net), [Technitium](https://technitium.com/dns/), etc,) to protect all network connected devices at once.

An online step-by-step editing guide is available with individual instructions for each hardware platform, here: https://www.techopedia.com/how-to/how-to-edit-your-hosts-files

PLEASE NOTE: For Wndows OS users (all versions), there is a size limitation on how large your HOSTS file can be, at roughly 1MB in total. Anything larger may have a major impact on connectivity and network performance overall.

## How to include/use the "HPT" blocklist(s) w/ DNS 'Sinkholes' (PiHole/Technitium/etc.)

Essentially there are both a static and a dynamic option to choose from, in using the HPT labeled files. You can download any/all files and select what entries to amend into your own blacklist as a simple copy/paste. That will allow you to statically add the entries listed as of the currently posted version.

If your choice of software or service allows you to periodically check for blacklist updates via URL, you may choose to dynamiclly access the raw files in this repository to refresh your included entries to the latest version available. Use the following address, and amend the full and accurate filename you require at the end, as: ```https://raw.codeberg.page/lumiworx/HPT-AI-Blocklist/retrieve-this-file.txt```. Within that string, simply replace "retrieve-this-file.txt" with whichever file name(s) you'll need to retrieve.

As an example of use... In Technitium, browse to your admin dashboard and then to the 'Settings' tab > then under the 'Blocking' sub-tab > and within it you'll find a settings block, titled 'Allow / Block List URLs'. It contains a multiline textbox, where you'll add each retrieval URL on a new line, and then click 'Save Settings'. to commit the modification.

> Make sure to select - or, keep enabled - "ANY Address" for the Blocking Type, if you currently have or will be adding additional HOST based resources for inclusion as blocklists.

![Technitium Admin - Allow / Block URL screenshot](https://codeberg.org/lumiworx/HPT-AI-Blocklist/raw/branch/main/images/Technitium-admin_external-blocklists.jpg "Technitium Admin - Allow / Block URL screenshot")

If you're scheduling external blocklist updates, the latest version(s) on the repository will be downloaded and merged into your active blocklist collection, according to whatever schedule you've set for that function. Please ensure that you have enabled scheduled updates and set an interval - then you will be able to initiate a manual retrieval/addition, if you choose to do it outside the scheduled time.

> NOTE: A significant portion of these listings were initially derived from a project intended
> for users of the uBlockOrigin and uBlocklist browser plugins. With
> Google's continued push to 'restrict' blocker/filter plugins for Chromium-based browsers, I've decided to
> drop all uB files from my own project, and focus on non-browser
> solutions that provide the same benefits, at the network level, that benefits all connected devices simultaneously. The original uB oriented project can be found here:
> https://github.com/laylavish/uBlockOrigin-HUGE-AI-Blocklist
