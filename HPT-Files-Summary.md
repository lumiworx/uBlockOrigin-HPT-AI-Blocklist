### This is a summary of what content you can expect to see in the HPT labeled 'AI-list' files

#### Comparing HPT file types

The **HPT-Full-AI-List** includes every entry collected thus far, and does not attempt to currate the entries into categories, or into the type of site or page or resource contained in it's listings. Essentially it is a proverbial 'kitchen sink' and 'baby with the bathwater' raw compilation of anything and everything related to AI. It is ***THE*** most heavy-handed approach to blocking what is AI related, as It contains all of the 'partial-' types as-described below, within the summary table.

The **HPT-partial-'xxx'-AI-List & HPT-dotART-AI-List** files are intended to group the entries into usage-types and better delineate what destinations might generate, or discuss or educate, or present the results of; AI content. These partial files allow you to selectively apply blocking based on your own individual criteria, while allowing some of those listed within the 'Full' list to pass filtering alltogether.

##### Note: Please do not combine any 'partial' file(s) with the 'Full' file, This will duplicate some or all entries, and is not beneficial. 

### HPT Filenames and contents

Filename  | Contents
------------- | -------------
HPT-dotART-AI-List | Indentified AI domains registered with an '.art' TLD
HPT-partial-Audio-AI-List | Indentified AI domains related to generative AI Audio Clips
HPT-partial-Explicit-AI-List | Indentified AI domains related to generative AI Explicit/Nude Images/Video
HPT-partial-Goog-Appl-AI-List | Indentified AI domains related to Official Google or Apple marketplace apps.
HPT-partial-Mixed-AI-List | Indentified AI domains containing some generative AI content and/or AI prompts products
HPT-partial-RootTLD-AI-List | Indentified, dedicated AI domains registered with an '.ai / .com / .co / .net / (Misc)' TLD
HPT-partial-aiUSERS-AI-List | Indentified AI domains related to users presenting their generative AI results

**Please note:** Three HPT file types summarized as 'Audio' and 'Image/Video' and 'Explicit' above may be relatable to producing and/or providing/distributing DeepFakes - including misrepresentable content some could classify as pornographic and/or unethical or illegal.